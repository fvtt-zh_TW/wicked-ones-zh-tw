Hooks.once('init', () => {
    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'wicked-ones-zh-tw',
            lang: 'zh-tw',
            dir: 'compendium'
        });
    }
});

